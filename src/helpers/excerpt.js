/**
 * Shorten a string as an excerpt and add a trailing ellipsis.
 *
 * @api public
 *
 * @param {string} text                      The string to format
 * @param {Object} options                   The helper option set
 * @param {number} [options.hash.length=100] The character length to limit to
 *
 * @return {string}
 */

const sanitize = require('sanitize-html');

module.exports = function (text) {
    return text.length > 100 ? sanitize(text, { allowedTags: [], allowedAttributes: {}}).slice(0, 150).replace(/\s\w+$/, '...') : sanitize(text);
};
