---
layout: index.hbs
title: "The Great Dark Horse: A short story"
description: "And thereupon came the great dark horse, who's hooves clattered through the long dark night..."
date: 23-01-2020
---

# The Great Dark Horse: A short story

And thereupon came the great dark horse, who's hooves clattered through the long dark night. They echoed the thunder in the sky. The fragments of the army seemed to fall upon the cold blackened earth. All men trembled when they saw the great dark horse, with mane and tail as black as the void.

And the great dark horse spoke upon the men. "Who are you that cometh in these great darknesses, servants of false gods? Whose foul breath doth burn the heart and tickle the soul. Scorch the nerves of the flesh, and stir the blood and warm the seed of the earth?"

For Akkadia, my armies and my troops, we were told of unending power, that was never seen or heard of again. Ancient empires and kingdoms tremble upon the ills of time. The ancient catacombs beneath me, full of untouched riches. I am empty of nothing except my thoughts, my thoughts are all that I am. A wasteland of empty skin, void of emotion, a wonderland of dreams.

The cold and stark light outside my tiny cell reminded me of the eternal night outside and I flinched. There is a certain wonder somewhere out in the perpetual darkness, though all I could see was the horrors of wretchedness. For weeks now have I slept locked away in this hall of memories, unheard by the thousand souls.

Before my trial, I was begging to be spared from this hell, yet the devoid one displayed perseverance. Who say they are there to help, though I knew they are lying, what they are there for is their own twisted self preservation. So I was wronged, though the punishment was justified in the eyes of the devoid one, who was swayed to the disgusting bane of mankind.

A piece of my soul was turned into a tainted wasteland, to be trampled underfoot by time and by the one's vile act. As the void has no laws, there is no law that the void cannot violate. Whereas death seems like an inevitability, even as the void is ignorant of its purpose and true desire. I am their vanguard, but alone am I not. For there have been more who have crossed the void and as such, those that come after await their death throes.