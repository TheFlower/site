---
layout: index.hbs
title: "The Deathmare"
description: "With honor and with beauty must the enemy witness that this is the promise that lights whole world"
date: 01-01-2012
---
# The Deathmare

## This was originally written in 2011 when I was a little angsty teen

Overcome the throng of vampires in the deathmare  
Finally succumb in death to the sleepless menace  
With honor and with beauty must the enemy witness  
That this is the promise that lights whole world  
As our breath of fire lasts eternally  
The high pitched shrieks of the eleven heads  
For here comes the god of all of creation  
The god who comes from skies above  
Speaks in a tongue of blood  
Once it was mirthless mockery  
But there was a momentary lapse  
And something still sounded almost dreamlike  
A hundred years of mourning from terrible tears  
Now it is deafening rage and shattering sadism  
And beneath the taunts of blasphemy all the world cowers  
And with the silver of an open eye my life flashed before my eyes  
And now the world dares not turn and look at me  
I have slain many sons of man  
And as they looked upon me  
I was slain with a look of cold disdain now my mind is free  
I know what they will say and how they will hurt me  
The taste of their tears is a dagger plunged into their hearts  
And as the cold winds carry their final screams  
Their hearts will not be heard anymore  