---
layout: index.hbs
title: "Radio Piracy in the Twente region: A story from the 70s and 80s"
description: "A story of radio piracy in the east of The Netherlands"
date: 20-03-2020
---

### This is a story as told by one of my uncles, who was, along with his brothers, active in the radio piracy scene. I've decided to transcribe it to English and post it here as there's a criminal lack of content on radio piracy, especially personal accounts.

___

I must've been about ten years old, when I got an electronics kit from my parents during Sinterklaas (_a holiday similar to Christmas_). With the parts from that kit, you could build all kinds of small electronics, such as an alarm or a blinking light. A micro FM transmitter was also part of the options. Of course, I quickly put that FM transmitter together. With a transistor, a bunch of resistors, an elco and a trimmer you got a real transmitter. The included crystal earphone functioned as a microphone. The microphone picked up sounds, so you could hear your own voice over the radio. The antenna consisted of a piece of wire, and you could just about receive the signal at the neighbor's house.

The big question was, "_How could I amplify the signal from the transmitter, and how do I play music on the radio?_" The latter was not so difficult, I made horn out of cardboard, which I then taped around the microphone and put the whole bastard in front of the loudspeaker of my pickup. (_we called these pickups back then, calling them turntables is a fairly modern naming_). Tadaaah! Music on the radio! Now, how to amplify the signal? Of course I had no clue how antennas worked, so I just did something. From a pair of a couple old steel stair treads I made a "V" shape, which I then attached the antenna wire to, and bingo! My signal reached Beuningen, a town about 2 kilometers away.

During that time, I had a whole lot of old singles (_45rpm vinyls_) from the 50s and 60s. One of the first songs I played, was "Banjo Boy", from Jan en Kjeld.

Shortly after, my brother Bennie gave me my first real transmitter. Even though it was another one-stage transmitter, so with one transistor, it was much more powerful so it reached way farther. I built a small mixer, bought a microphone and came up with my first pirate radio station name: "Radio Twentse Rakker".

After my transition from primary school to the LTS in Oldenzaal (LTS is Lower Technical School, a type of high school), I was placed in a class with someone from a certain Horsthuis from Rossum. This must've been the Horsthuis of the current RTV store in Rossum. From him, I bought a new transmitter for 10 guilders. For the radio freaks among you, this was a balanced transmitter with 2x 2n2219A transistors. I think about one watt of power came out of the thing, at that time, you quickly reached 20 kilometers with that kind of power.

![Transmitter Schematic](https://owo.sh/7rfvCxW.jpg)

(the transmitter with the 2N2219A transistors)

The peripherals were also expanded, as I built a new mixer and bought a new microphone and pickups. It started to look like a real radio station. The mixer was no bigger than a carton of milk, but there were 3 volume sliders for speech and music and 2 controls for tone. That's how it was at the time. Quite quickly I started to learn how to build my own transmitters, as a starting point a balanced transmitter with 2 transistors. The 2N3536 turned out to work best, those transistors quickly threw about 3 watts. You will hardly believe it, but with a good directional antenna you could reach the southern end of Drenthe! Try that nowadays!

Around the age of 14, is when I started playing music for the public. Namely at the Vledderhoes in Denekamp, for the youth of the town. That's when the DJ in me was born.

At that time, in the 80s, there were many pirate radio stations in Denekamp. Actually, those years were really the glory years of the ether pirate, you had one at probably every street corner, just casually in the built up areas of town. Illustrious names such as Radio Atletico, Alaska, Mi Amigo, Paradiso, Sacramento, Merry Ellis, Delmare, Biscaya (we haven't forgotten you Bets!) and White Falcon flung their tunes into the ether every day. I myself changed my pirate name to "Radio Atlantis". For a few years, I broadcasted from my childhood home (where I still live today) under that name. Later, the station moved to the attic where I spent many hours with my brothers Rudi and Eddy. Mostly with friends who could just walk straight in to our house from my parents. My parents thought it was very nice to have all those friends and fellow pirates over. Today, if I go up into the attic, the wall still shows the names of the pirates who were in our attic at the time. After all these years, a precious memory.

Shortly thereafter, I not only played under my own pirate radio name, but also with Radio Noordzee. The studio of Radio Noordzee was located above the workshop of contractor Meijer in Denekamp. There, we had our own place and a fantastic studio. We kept it up for quite a long time. At that time, Radio Noordzee was a really close group of friends who hung out primarily on the weekends. Going out to clubs and pubs, the pirate radio and girls is what we spent most of our hours on, and that worked out pretty well.

<img src="https://owo.sh/9UCiJS7.jpg" style="max-width:70rem">

We even built our own carnaval floats and participated in the parade in Denekamp and De Lutte. The theme of the float was "Pisa", the TV program in those years by Harry Vermeegen and Henk Spaan. The toilet pots that were scattered all over the road from Oldenzaal to Denekamp after the parade came from our float. It wasn't me :)

In 1981, a number of radio pirates, including me, spent 3 days on a family swing at cafe 't Wubbenhof in Denekamp. A mention in the Guiness Book of Records as a European record was our reward.

<img src="https://owo.sh/4VbBu1s.jpg" style="max-width:70rem">

After I started living on my own in Denekamp, of course the radio piracy continued. However, the real glory years were over. The police raided more often, which meant that in no time was I arrested again, for the 5th time, because of illegal broadcasting. The highlight was the raid with a whole armada of police officers, confiscating my entire record collection. Fortunately I got it back a year later.

At that time, I also met the still well-known ether pirate Rob (o. G.) from Denekamp. At his home, in his garden shed we spent many hours together, which was just behind the police station too! We even got caught in the middle of the night, the officers were at the police station and they listened to us on the radio. Because they had nothing to do anyway, they decided to arrest us, after all, we were only a couple hundred meters away.

In 1988, I got into a relationship with Elise, and then the fun was over. The radio piracy out, or I out! She was right, fines already rose to 500 guilders. Fortunately, the local radio station, Radio Denekamp Lokaal, came my way. I had been involved with RDL (the current Twente FM) from the beginning. Every Sunday evening it was time for "Adjes Stamkroeg", the nickname Adje comes from my pirate time. I called myself "Adje Snoet'n Roland", after the famous DJ Adje Roland. People who know me from the past still call me that sometimes.

Years later, the Stamkroeg program was continued on Sunday morning at TwenteFM, where I presented "Pluk de dag" together with Elise, that was super fun. Unfortunately, after about 3 years, the new board of directors kicked us out because our program would no longer fit with the station's vision, that was after 16 years of loyal service.

Fortunately, there was a spot available at AccentFM in Oldenzaal, so my program was continued there. Unfortunately, it was a troublesome time for the station and after the announcement of a merger between TwenteFM and AccentFM, we chose the best way out, just stop. That's how my DJ era came to an end, I thought. At the time, I was also a regular DJ at various parties. For more than 10 years, I was a regular at the carnival association "De Nachtuulkes" in Denekamp. Later joined by my son Brian, who now works as a DJ.

They sometimes say, "once a DJ, always a DJ". That's right. The blood is thicker than water. I do keep regretting that I no longer have a radio program, I still like it very much, but who knows what'll come my way.

I conclude with the words I always used on the radio. Inspired by sun, moon, stars and love, I was happy to do it for you.