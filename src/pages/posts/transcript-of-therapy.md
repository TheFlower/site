---
layout: index.hbs
title: "Transcript of therapy sessions: Highlights"
description: "This is the first part in a series of transcripts of my therapy sessions that started in November 2019."
date: 24-01-2020
---

# Transcript

Therapist: "How do you feel?"

Me: "Today? Or in general?"

Therapist: "Today."

Me: "I don't know.. I'm just, it's a lot of high highs and low lows. I'm all over the place."

Therapist: "Do you know why?"

Me: "I'm not sure, there's moments where I'm really confident and doing a lot of things, like, I'm out a lot and active with the interviews, so that feels really good and makes me feel confident.. So that's good that I can do that.. But on other days I'm really exhausted and I feel useless, because there's so many things running through my mind still and I don't know how to fix that."

Therapist: "Have you tried anything to fix that?"

Me: "I've been like trying to go back to my roots.. I don't know.. I do feel like smoking weed helps me a lot like.. It's hard to like, when I wake up in the morning, I don't know which version of me is going to show up.. Like when I'm on weed I'm consistently like.. Better? So then I'm like, maybe I could.. Maybe I could not smoke it and see if I can do better? But.. Every single time I smoke I do always feel consistently better.. I hasn't happened when I don't smoke."

Therapist: "It's good that it makes you feel better, obviously I can't recommend you taking drugs but I can't stop you and if it helps you temporarily, all the better. You said there's things running through your mind, is there anything in specific?"

Me: "Just.. I don't know, I feel like I need closure about my ex? It just.. Like I talked, I talked like to everyone right and it makes sense but it is hard to accept that's the way it is."

Therapist: "Everyone struggles with that in any type of breakup and you're not alone in finding it hard to accept the situation for what it is, it's irrelevant of her, there isn't a need for you to find acceptance in the way *she* sees things, acceptance doesn't work that way, you need to find your own peace."

Me: "I suppose but, I feel like, this is an extreme, especially in the current year.."

Therapist: "Certainly, and you're very strong that you're dealing with this in the way you have. So, like you mentioned, you feel the need to find closure, would it be helpful if we went over the different disorders and try to find a clearer answer? I've talked about borderline personality disorder with you before, but do you feel like there's a piece missing?"

Me: "Definitely, I do feel like there's a piece missing, like we talked about aspergers and her patterns but I feel like it's just one piece of the puzzle, you know?"

Therapist: "You already know it wasn't you. There's not much you can do with the exact information about what she did. Knowing it's borderline, it doesn't matter, she's unwilling to see professional help for it, you may feel responsible because you're aware of this problem, but from what you said I don't think you're still attracted to her but I think you're retaining an emotional attachment.
A resistance to truly, permanently ending everything because you want her to get help.
You're not responsible, Luna, she's an adult. It would be nice if she was receptive to help if she agreed that what she did was awful and she needs help and you could be there emotionally to guide, but she does not! And you know she won't listen to anyone about her toxic behavior, and you know she'll just keep projecting and projecting.
Luna, not only are you not responsible but you are not able to help her by the very nature of who she is, because of her borderline disorder, that's no fault of your own."

Me: "I feel at fault because I didn't know about her issue until after she left, I could've gotten her help."

Therapist: "There is nothing to be gained, she will go off and do this to the next person and be like this until she -really- messes up something and then maybe she'll get help. It will take an emotional event more pertinent to her. You can only be at fault if you take yourself to be her caretaker, but I do not believe good love ought to be that way... You should certainly be nice and emotionally supportive and everything, but you can not be her emotional base. Borderlines need to be stable in themselves with professional help before they can have a good relationship, this next one she's in isn't any different and she's just fooling herself more and you know that. Because then, if not, those responsibilities start to fall on other people, like you Luna, and now you're left fretting over how you didn't manage to take care of her perfectly. And it's inherent in that situation that you can't take care of her perfectly because she has borderline personality disorder."

Me: "But she doesn't feel or understand emotions like you said, everything is empty, she's nothing, because borderlines just mirror their personalities."

Therapist: "Yes and that is really, really unfortunate, and it's awful that some people go through life like that! But that is all that can be done really... I know there are lots of "but"s and she really is going to screw some stuff up and she -really- needs help, but there is nothing you can do.
Nobody can be tethered to somebody like this and live well Luna, not even you. I really hope you recognize that. None of her relationships will ever last, nor will yours ever reform. Nor should you hope so. If you were unable to fix it while literally married to her there's not much you can do. She's incredibly messy and mental illness often self-preserves. You will not be able to fix her because she is incapable of receiving help by the very nature of her brain. She will have to be her own impetus for change, you know that and there's nothing to be done but care for yourself."

Me: "I guess that makes sense, yeah.."

Therapist: "Knowing the world is a very important step in accepting it, knowing perfectly in your head and your heart that she is messy and it's not your job to fix it, nor could you regardless of your talents... It makes it easy to just accept it and turn to healing from everything, to building yourself up again."

Me: "A big part of it is that she's the only person who was, for a while, not abusive to me, obviously I learned later that her borderline behaviors are abusive but still.."

Therapist: "Oh I'm so sorry Luna, that's a good memory to have, I'm sorry that it is tainted by her disorder..."

Me: "I'm having a hard time separating the person she was to me, to the person she really is deep down, especially that it's been a trend throughout my life with my dad for example being a narcissist, there's so much pain."

Therapist: "A sad trail, but such a wonderful person to be born from it all though. I'm sorry that your emotional history is so very pertinent. I'm very familiar with the feeling of everything going wrong, struggling to hold the skeleton of life, especially your PTSD, I don't understand exactly how it feels to associate loved ones with those negative emotions such as what happened in your childhood and the feeling to be abandoned by her in that manner, the trauma, physical abuse from your childhood but I do understand how it affects you. It's certainly not a do-nothing problem, PTSD flaring will inevitably force help for it, which you're getting with a psychoanalyst! Trauma rewires your brain and rewiring it back to normal takes surgeon-like hands."