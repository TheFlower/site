#!/bin/bash

# When we get killed, kill all our children
trap "exit" INT TERM
trap "kill 0" EXIT

# Source in util.sh so we can have our nice tools
. $(cd $(dirname $0); pwd)/util.sh


nginx -t && service nginx reload

echo "Done with startup"

while [ true ]; do
    echo "running certbot"

    sleep 604810 &
    SLEEP_PID=$!
    wait "$SLEEP_PID"
done