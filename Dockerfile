FROM nginx:latest
RUN apt update && \
  apt install -y python python-dev libffi6 libffi-dev libssl-dev curl build-essential && \
  apt install -y python3-pip && \
  pip3 install -U pip && \
  pip install -U cffi certbot && \
  apt remove --purge -y python-dev build-essential libffi-dev libssl-dev curl && \
  apt-get autoremove -y && \
  apt-get clean

WORKDIR /site
COPY package.json /site
RUN apt update -q
RUN apt install -y nodejs npm && npm i -g yarn
RUN node -v
RUN yarn
COPY src /site/src
COPY metalsmith.json /site
COPY postcss.json /site
RUN yarn run build
RUN mkdir /var/www
RUN cp -r /site/build /var/www/flower.wtf
RUN cp -r /site/src/assets/pgp /var/www/flower.wtf/assets/pgp

COPY ./deploy/flower.wtf.conf /etc/nginx/conf.d/flower.wtf.conf

EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"]